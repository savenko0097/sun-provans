import './index.html';
import './styles/style.scss';
import headerHTML from './html/_header.html';
import homeHTML from './html/_home.html';
import footerHtml from './html/_footer.html';

// add _html to index.html 
const headerDiv = document.getElementById('header');
headerDiv.innerHTML = headerHTML;
const mainHome = document.getElementById('main');
mainHome.innerHTML = homeHTML;
const footerDiv = document.getElementById('footer');
footerDiv.innerHTML = footerHtml;
// hamburger menu
const hamburger = document.querySelector(".hamburger");
const navigation = document.querySelector(".header__navbar-info");
hamburger.addEventListener("click", mobileMenu);
function mobileMenu() {
  hamburger.classList.toggle("active");
  navigation.classList.toggle('active');
}

// video play
// const video = document.getElementById('header-video');

// video.play();

// slider__home
$(document).ready(function () {
  $('.home__slider').slick({
    arrows: true,
    dots: true,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    // fade: true,
    cssEase: 'linear',
    responsive: [{
      breakpoint: 768,
      settings: {
        speed: 800
      }
    }, {
      breakpoint: 480,
      settings: {
        speed: 900
      }
    }]
  });
});

// $('.one-time').slick({
//     dots: true,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     adaptiveHeight: true
//   });

// //
// let prev = document.querySelector('.slick-prev');
// prev.innerHTML = "<i class='bx bx-chevron-left'></i>";

// //GEO location
// function initMap() {
//     // Координаты места
//     const location = { lat: 49.413796, lng: 26.953709 };

//     // Создание карты
//     const map = new google.maps.Map(document.getElementById("map"), {
//       center: location,
//       zoom: 15,
//       disableDefaultUI: true, // Отключение элементов управления (например, кнопки увеличения/уменьшения масштаба)
//     });

//     // Добавление маркера
//     const marker = new google.maps.Marker({
//       position: location,
//       map: map,
//     });
//   }